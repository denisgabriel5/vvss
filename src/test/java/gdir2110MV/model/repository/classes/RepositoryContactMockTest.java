package gdir2110MV.model.repository.classes;

import gdir2110MV.Service.Service;
import gdir2110MV.exceptions.InvalidFormatException;
import gdir2110MV.model.base.Contact;
import gdir2110MV.model.repository.interfaces.RepositoryActivity;
import gdir2110MV.model.repository.interfaces.RepositoryContact;
import gdir2110MV.model.repository.interfaces.RepositoryUser;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RepositoryContactMockTest {
    private RepositoryContact repo;
    private RepositoryActivity repoActivity;
    private RepositoryUser repoUser;
    private Service service;

    @Before
    public void setUp() throws Exception {
        this.repo = new RepositoryContactMock();
        this.repoActivity = new RepositoryActivityMock();
        this.repoUser = new RepositoryUserFile();
        this.service = new Service(repo, repoUser, repoActivity);
    }

    @Test
    public void addContact1() throws InvalidFormatException {
        boolean retValue = false;
        Contact c = new Contact("George", "Str.Cimpului", "+40728457987");
        retValue = this.repo.addContact(c);
        assertEquals(true, retValue);
    }
    @Test
    public void addContact2() throws InvalidFormatException {
        boolean retValue = false;
        Contact c = new Contact("Geo", "Str.Cimpului", "+40728457985");
        retValue = this.repo.addContact(c);
        assertEquals(true, retValue);
    }

    @Test
    public void addContact3() throws InvalidFormatException {
        boolean retValue = false;
        Contact c = new Contact("Ge", "Rasaritului", "+1212345");
        retValue = this.repo.addContact(c);
        assertEquals(false, retValue);
    }

    @Test
    public void addContact4() throws InvalidFormatException{
        boolean retValue = false;
        Contact c = new Contact("George", "Str Cimpului","+4012345");
        retValue = this.repo.addContact(c);
        assertEquals(true, retValue);
    }
    @Test
    public void addContact5() throws InvalidFormatException{
        boolean retValue = false;
        Contact c = new Contact("George", "Str. Cimpului", "-58796487");
        retValue = this.repo.addContact(c);
        assertEquals(false, retValue);
    }
}